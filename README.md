# Challenge-rooftop



## Steps to runs the project
Execute the next command to run the project:
```
git clone git@gitlab.com:juanmartin_796/challenge-rooftop.git
cd challenge-rooftop
npm install
export CHALLENGE_EMAIL=SOME_EMAIL
npm run start
```

## Run Tests
Execute the next command to runs the tests.
```
npm run test
```