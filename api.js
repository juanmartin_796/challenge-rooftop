import axios from "axios";

const check = async (token, string1, string2) => {
    const url = `https://rooftop-career-switch.herokuapp.com/check?token=${token}`;
    try{
        const res = await axios.post(url, {blocks: [string1, string2]});
        return(res.data.message)
    }
    catch(err) {
        console.log("something was wrong!", err)
    };
}

const checkResult = async (array, token) => {
    const url = `https://rooftop-career-switch.herokuapp.com/check?token=${token}`;
    try{
        const encoded =  array.join('')
        const res = await axios.post(url, {encoded});
        return(res.data.message)
    }
    catch(err) {
        console.log("something was wrong!", err)
    };
}

const getToken = async (email) => {
    const url = `https://rooftop-career-switch.herokuapp.com/token?email=${email}`;
    try{
        const res = await axios.get(url);
        return(res.data.token)
    }
    catch(err) {
        console.log("something was wrong!", err)
    };
}

const getBlocks = async (token) => {
    const url = `https://rooftop-career-switch.herokuapp.com/blocks?token=${token}`;
    try{
        const res = await axios.get(url);
        return(res.data.data)
    }
    catch(err) {
        console.log("something was wrong!", err)
    };
}

export const api = {
    check,
    checkResult,
    getToken,
    getBlocks
}