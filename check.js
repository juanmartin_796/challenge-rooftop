import { api } from "./api.js"

export const check = async (blocks, token) => {
    if (blocks.length < 3) {
        return blocks
    }
    let copyBlocks = [...blocks] // Clone blocks to avoid modify it
    let prevBlock = copyBlocks[0]
    const resultArray = [copyBlocks.shift()] // Init resultArray with the first element of the array because there is in the correct position and remove it from copyBlocks
    const numberIteration = copyBlocks.length - 1 // -1 because for the lastone element we don't need call the API

    for (let i = 0; i < numberIteration; i++) {
        let foundSeq = false
        const iterationSearchSeq = copyBlocks.length - 1 // -1 because for the last element we can deduce it is the next on the sequence 
        for (let j = 0; j < iterationSearchSeq; j++) {
            const currentBlock = copyBlocks[j]
            const areSequential = await api.check(token, prevBlock, currentBlock)
            if (areSequential) {
                resultArray.push(currentBlock);
                copyBlocks = copyBlocks.filter(block => block !== currentBlock) // Remove currentBlock from the blocks, because  we know your correct position now
                prevBlock = currentBlock
                foundSeq = true
                break;
            }
        }
        if (!foundSeq) { // If we don't find the next sequence string in the n-1 elements of blocks array used in the previous FOR, we can deduce the last element is the next on the sequence
            const lastBlock = copyBlocks[copyBlocks.length - 1]
            resultArray.push(lastBlock);
            copyBlocks = copyBlocks.filter(block => block !== lastBlock) // Remove lastBlock from the array to search the sequential
            prevBlock = lastBlock
        }
    }
    resultArray.push(...copyBlocks) // We add the last element in blocks to the resultArray because it is the only one left and there is no need to call the api
    return resultArray
}
