import { api } from "./api.js"
import { check } from "./check.js"

const mail = process.env.CHALLENGE_EMAIL
console.log("Your email is: ", mail)

const token = await api.getToken(mail)
const blocks = await api.getBlocks(token)
console.log("Unsorted blocks: ", blocks)
console.log("Processing data, wait a few seconds please...")
const sortedBlocks = await check(blocks, token)
console.log("Sorted blocks: ", sortedBlocks)
console.log("Is correct?: ", await api.checkResult(sortedBlocks, token))