import { check } from "../check.js";
const nock = require('nock')

const token = "some_token"

const testValues = [
    {
        input: [],
        expected: []
    }, {
        input: ['1'],
        expected: ['1']
    }, {
        input: ['1', '2'],
        expected: ['1', '2']
    }, {
        input: ['1', '3', '2'],
        expected: ['1', '2', '3']
    }, {
        input: ['1', '2', '3', '4', '5', '6', '7', '8', '9'],
        expected: ['1', '2', '3', '4', '5', '6', '7', '8', '9']
    }, {
        input: ['1', '9', '8', '7', '6', '5', '4', '3', '2'],
        expected: ['1', '2', '3', '4', '5', '6', '7', '8', '9']
    }, {
        input: ['0', '1', '5', '2', '6', '4', '8', '3', '9', '7'],
        expected: ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9']
    }
]

describe("Check function", () => {
    test("The first element of the result should be the first element of the input array", async () => {
        for (let value of testValues) {
            mockCheckApi()
            const sortedBlocks = await check(value.input, token)
            expect(sortedBlocks[0]).toStrictEqual(value.expected[0])
        }
    })

    test("Should return an array", async () => {
        for (let value of testValues) {
            mockCheckApi()
            const sortedBlocks = await check(value.input, token)
            expect(Array.isArray(sortedBlocks)).toBe(true)
        }
    })
    test("Should return an array with the same length of the input", async () => {
        for (let value of testValues) {
            mockCheckApi()
            const sortedBlocks = await check(value.input, token)
            expect(sortedBlocks.length).toBe(value.input.length)
        }
    })

    test("Should return the sorted blocks", async () => {
        for (let value of testValues) {
            mockCheckApi()
            const sortedBlocks = await check([...value.input], token)
            expect(sortedBlocks).toStrictEqual([...value.expected])
        }
    })
})

// Function to mockup the response data of Check request
const mockCheckApi = () => {
    const validValues = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9']
    for (let i of validValues) {
        for (let j of validValues) {
            const body = {
                blocks: [i, j]
            }
            nock('https://rooftop-career-switch.herokuapp.com')
                .post(`/check?token=${token}`, body)
                .reply(200, { message: Number(j) - Number(i) === 1 })
        }
    }
}